using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Link : MonoBehaviour
{
    private Button btn;
    public string link;
    // Start is called before the first frame update
    void Start()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() =>
        {
            Application.OpenURL(link);
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
