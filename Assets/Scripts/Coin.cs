﻿using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
	static public Pooler coinPool;
    public bool isPremium = false;
    public List<GameObject> utils;

    private void OnEnable()
    {
        if (utils.Count > 0)
        {
            foreach (var item in utils)
            {
                item.SetActive(false);
            }
            utils[Random.Range(0, utils.Count)].SetActive(true);
        }
    }
}
