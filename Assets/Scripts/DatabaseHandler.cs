using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class DatabaseHandler : MonoBehaviour
{
    [Header("Input References")]
    public TMP_InputField playerName;
    public TMP_InputField id;
    public TMP_InputField email;
    public TMP_InputField date;
    public TMP_InputField mobile;
    public TMP_InputField password;
    public TMP_InputField invoice;
    public TMP_InputField nickname;
    public TMP_InputField signInEmail;
    public TMP_InputField signInPassword;
    public Toggle mayor;
    public Toggle afiliado;
    public Toggle cencosud;
    public Toggle olimpica;
    public UnityEvent nextScreenCondiciones;
    public UnityEvent onSignUpSuccesfull;
    public UnityEvent onSignInSuccesfull;
    public UnityEvent onSignUpFailed;
    public UnityEvent onSignInFailed;
    private string mall = "cencosud";

    public string currEmail;

    // Update is called once per frame
    public void TryToSignUp()
    {
        if ((!playerName.text.Equals("")) && (!id.text.Equals("")) && (!email.text.Equals("")) && (!nickname.text.Equals("")) && (!mobile.text.Equals("")) && (!password.text.Equals("")))
        {

            StartCoroutine(SendData());
        }
        else
        {

            onSignUpFailed.Invoke();
        }
    }
    public void CheckMall(string _mall)
    {
        if (_mall.Equals("cencosud"))
        {
            olimpica.isOn = false;
            mall = "cencosud";
        }
        else
        {
            mall = "olimpica";
            cencosud.isOn = false;
        }
    }
    public void CheckToggles()
    {
        if (mayor.isOn && afiliado.isOn && (olimpica.isOn || cencosud.isOn))
        {
            nextScreenCondiciones.Invoke();
        }
    }
    IEnumerator SendData()
    {
        Debug.Log("sending data");
        WWW conn = new WWW("http://temporadaescolarcompensar.com/php/signup.php?name=" + playerName.text + "&mobile=" + mobile.text + "&nick=" + nickname.text + "&email=" + email.text + "&date=" + date.text + "&invoice=" + invoice.text + "&pass=" + password.text + "&mall=" + mall + "&id=" + id.text);
        yield return (conn);
        if (conn.text.Equals("401"))
        {
            onSignUpSuccesfull.Invoke();
            currEmail = email.text;
        }
        else
        {
            onSignUpFailed.Invoke();

        }
        Debug.Log(conn.text);
    }
    public void TryToSaveScore(int _score)
    {
        StartCoroutine(SendScore(_score));
    }
    IEnumerator SendScore(int _score)
    {
        WWW conn = new WWW("http://temporadaescolarcompensar.com/php/sc.php?a=" + currEmail + "&b=" + _score);
        yield return (conn);
        if (conn.Equals("401"))
        {
            onSignUpSuccesfull.Invoke();
        }
        Debug.Log(conn.text);
    }
    public void TryToLogin()
    {
        StartCoroutine(Login());
    }
    IEnumerator Login()
    {
        WWW conn = new WWW("http://temporadaescolarcompensar.com/php/signin.php?a=" + signInEmail.text + "&b=" + signInPassword.text);
        yield return (conn);
        if (conn.text.Equals("401"))
        {
            Debug.Log(conn.text);
            currEmail = signInEmail.text;
            onSignInSuccesfull.Invoke();
        }
        else
        {
            onSignInFailed.Invoke();
        }
        Debug.Log(conn.text);
    }
}
