using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreen : MonoBehaviour
{
    public float duration = 1;
    // Start is called before the first frame update
    private void OnEnable()
    {
        StartCoroutine(WaitToRender());
    }

    IEnumerator WaitToRender()
    {
        yield return new WaitForSecondsRealtime(duration);
        gameObject.SetActive(false);
    }

}
