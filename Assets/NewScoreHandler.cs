using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NewScoreHandler : MonoBehaviour
{
    public TextMeshProUGUI finalScoreText;
    public TrackManager trackManager;
    public int finalScore;
    public int scoreWithoutPickups;
    public int coins;
    public DatabaseHandler dbHandler;
    public CharacterInputController controller;
    private void OnEnable()
    {
        coins = controller.coins;
        finalScore = trackManager.score + (coins * 6);
        scoreWithoutPickups = trackManager.score;
        finalScoreText.text = finalScore.ToString();
        dbHandler.TryToSaveScore(finalScore);
    }
}
